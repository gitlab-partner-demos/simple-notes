---
title: Introduction
type: docs
---

# Welcome

Welcome to Simple Notes - GitLab DevSecOps Introduction. This Project will help you gain a better understanding of how to successfully
shift security left to find and fix security flaws during development and do so more easily, with greater visibility and control than
typical approaches can provide.

## Getting Started

In order to get started, go through each of the lessons described within the workshop:

[Lesson 1: Prerequisites](./getting_started/lesson_1_prerequisites/)  
[Lesson 2: Deploying the Demo Application](./getting_started/lesson_2_deploying_the_demo_application/)  
[Lesson 3: Setting up and Configuring the Security Scanners and Policies](./getting_started/lesson_3_setting_up_and_configuring_the_security_scanners_and_policies/)  
[Lesson 4: Developer Workflow](./getting_started/lesson_4_developer_workflow/)  
[Lesson 5: AppSec Workflow](./getting_started/lesson_5_appsec_workflow/)  

## Outcomes

- How to achieve comprehensive security scanning without adding a bunch of new tools and processes
- How to secure your cloud native applications and IaC environments within existing DevOps workflows
- How to use a single-source-of-truth to improve collaboration between dev and sec
- How to manage all of your software vulnerabilities in one place
- How to automate and monitor your security policies and simplify auditing
- How to detect unknown vulnerabilities and errors using fuzz-testing

## Sections

| # |     Title     |                Description                   |
| - |---------------|----------------------------------------------|
| 1 | Prerequisites | Requirements to get started with the project |
| 2 | Deploying the Demo Application | Learn how to deploy and expose the demo application |
| 3 | Setting up and Configuring the Security Scanners and Policies | Learn how to setup and configure the different types of security scans. This includes Security Policies as well |
| 4 | Developer Workflow | Learn how to view and take action on vulnerabilities within a Merge Request |
| 5 | AppSec Workflow | Learn how to triage vulnerabilities and collaborate with other members of a Security team |

## Additional Resources

To learn about the project we are using you can see the following documentation:

- [Project Architecture](./documentation/architecture/)
- [Development Guide](./documentation/development_guide/)
- [Contributing](./documentation/contributing/)
