# Simple Notes (Security Demo Application) 🔒

---

## **Note: Do not edit this project directly, but rather clone it and configure it within your own GitLab instance.**

This application is used for taking simple notes, like `Everyone needs to go ahead and come in on Saturday and Sunday to help out with the new code push`. It is used to demo many of GitLab's Security features. Get started by viewing the [**Simple Notes Documentation**](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/)!

![](./images/screenshot.png)  

If there is something you wanna see featured in this application, please open an issue! MRs are welcome 😁

---

## Tutorial / Lessons

You can get started using this project on your own GitLab account with the following lessons:

- Lesson 1: [Prerequisites](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/getting_started/lesson_1_prerequisites/)
- Lesson 2: [Deploying the Demo application](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/getting_started/lesson_2_deploying_the_demo_application/)
- Lesson 3: [Setting Up and Configuring the Security Scanners and Policies](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/getting_started/lesson_3_setting_up_and_configuring_the_security_scanners_and_policies/)
- Lesson 4: [Developer Workflow](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/getting_started/lesson_4_developer_workflow/)
- Lesson 5: [AppSec Workflow](https://tech-marketing.gitlab.io/devsecops/initech/simple-notes/getting_started/lesson_5_appsec_workflow/)

---

## Useful Resources

- [GitLab Ultimate Pricing Page](https://about.gitlab.com/pricing/ultimate/)
- [DevOps Solution Resource - DevSecOps](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/)
- [What is DevSecOps?](https://about.gitlab.com/topics/devsecops/)
- [Integrating Security into your DevOps workflow](https://about.gitlab.com/solutions/dev-sec-ops/)
- [GitLab Secure Direction Page](https://about.gitlab.com/direction/secure/)
- [GitLab Govern Direction Page](https://about.gitlab.com/direction/govern/)

---

Created and maintained by [Fern](https://gitlab.com/fjdiaz)🌿

- [LinkedIn](https://www.linkedin.com/in/awkwardferny/)
- [Twitter](https://twitter.com/awkwardferny)
